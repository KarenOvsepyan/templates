//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ___VARIABLE_sceneName___Info: Info  {
    init(delegate: InterfaceItemDelegate?) {
        super.init(delegate: delegate)
    }
}

class ___VARIABLE_sceneName___Model: RouterModel {
    
    
    override init() {
        super.init()
    }
    
    override func allDataLoaded() {
        super.allDataLoaded()
    }
    
}
